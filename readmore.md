# Wg Simple API

GET STARTED : GO TO APP ROOT, THEN RUN IN YOUR TERMINAL
$ sh run.sh

THIS IS A SIMPLE API FOR TESTS PURPOSES. THIS API IS CONSTRUCTED
USING THE MICROFRAMEWORK FLASK

http://flask.pocoo.org/docs/1.0/

Python 2.7.10
pip[x] install flask

---------------------------------------------------------------

PREFER START USING .run.sh IN THE APPLICATION ROOT
$ sh run.sh

TO START THIS API FROM YOUR OWN 
RUN THE FOLLOWING COMMAND ON WG-API ROOT DIR
$ FLASK_APP=api.py flask run

TO BE VISIBLE BY YOUR OWN NETWORK
$ FLASK_APP=api.py flask run --host=0.0.0.0

HOWEVER, PREFER TO USE THE SH FILE

---------------------------------------------------------------

TO INCLUDE NEW INFO ON THIS API FOLLOW THIS STEPS:

1.0  CREATE A NEW JSON WITH YOU DATA AT ./data
2.0  CREATE A SIMPLE VAR WITH readJson( your_json_file ) at Api class on collection.py
2.1  YOU CAN CREATE YOUR OWN SET OF DATA ON EXTEND CLASSES ON THE collection.py
3.0  IMPORT YOUR CLASS FROM COLLECTION IN THE api.py ( from collection import Api )
4.0  RETURN YOUR DATA WITH return jsonify(Api.yourdata)

---------------------------------------------------------------

FILES

api.py : contains the flask api routes and data returned
collection.py : class collection that reads jsons files in data
functions.py : common functions to help all process
run.sh : used to start 
test.py : file to test things out