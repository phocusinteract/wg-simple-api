from functions import *

class Api:
	# information about this api
	info = readJson("data/info.json")

	# twitter api response example
	# https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html
	twitter = readJson("data/twitter.json")

	# nobel prizes
	nobels = readJson("data/nobels.json")

	# brazillian population
	brazilpop = readJson("data/brazilpop.json")