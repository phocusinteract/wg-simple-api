from time import sleep
from flask import Flask
from flask import request
from flask import jsonify
from collection import Api

app = Flask(__name__)

@app.before_request
def my_method():
	# get the /sleep param
	# pass the ?sleep=x url param where x is time is seconds
	s = request.args.get('sleep')
	if s:
		sleep(int(s))

# endpoints ----------------------

@app.route("/")
def info():
    return jsonify(Api.info)

@app.route("/twitter")
def twitter():
	return jsonify(Api.twitter)

@app.route("/brazilpop")
def brazilpop():
	return jsonify(Api.brazilpop)

@app.route("/nobels")
def nobels():
	return jsonify(Api.nobels)	