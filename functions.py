import json

# read a json from data and returns an array
def readJson( file ):
	with open( file ) as f:
		data = json.load(f)
	return data	