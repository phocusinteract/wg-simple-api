# Wg Simple API

A simple Python No-Database API using Flask. This is an API for development test purposes.
This API runs locally without any kind of database connection. The endpoints simply returns
the data from locally json files with a ?sleep=X function implemented.

# Get Started 

0. have Python 2.7^ installed
1. Install flask (pip[3] install flask);
3. Go to app root folder and run "sh run.sh"
4. Now open your localhost:5050 and voilá

# Sleep Option

If you need to test the response time, loading views, timeouts etc, you can use the ?sleep param:

localhost:5050/yourendpoint/?slee=s 

where s = time in seconds. So, the api will wait s seconds to return the data.

# Current Endpoints

There are 4 basic endpoints and you can add as many as you need:

/info 	   - returns info about the api
/nobels    - a list with nobel prize winners
/twitter   - a example of twitter api response (api 1.1)
/brazilpop - current habitants count in brazil

# Go Further

READ THE "startpoint.txt" FILE TO FURTHER INFORMATION ABOUT HOW TO ADD NEW ENDPOINTS.